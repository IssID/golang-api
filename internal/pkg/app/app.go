package app

import (
	"app/internal/app/endpoint"
	"app/internal/app/middleware"
	"app/internal/app/service"
	"fmt"
	"github.com/labstack/echo/v4"
	"log"
)

type App struct {
	endpoint *endpoint.Endpoint
	service  *service.Service
	echo     *echo.Echo
}

func New() (*App, error) {
	app := &App{}

	app.service = service.New()
	app.endpoint = endpoint.New(app.service)

	app.echo = echo.New()

	app.echo.Use(middleware.RoleCheck)
	app.echo.GET("/status", app.endpoint.Status)

	return app, nil
}

func (app *App) Run() error {
	fmt.Println("Server running")

	err := app.echo.Start(":8080")
	if err != nil {
		log.Fatal(err)
	}

	return nil
}
